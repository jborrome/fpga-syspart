import os
import math
import sys
from absl import flags
from multiprocessing import Pool
from functools import partial
from time import sleep

import parse_config as p_cfg
import sram_traffic_ws as base_trace
import conv_ultrascale_bram_trace as bram_trace
import conv_ultrascale_uram_trace as uram_trace
import trace_analysis as analysis

def setup_log_file_storage(dirName):
    # Create target directory & all intermediate directories if don't exists
    try:
        os.makedirs(dirName)    
    except FileExistsError:
        os.system('rm -rf '+dirName+"/*")

def clean_trace_file_storage(dirName):
    # Create target directory & all intermediate directories if don't exists
    os.system('rm -rf '+dirName)

def run_simulation(
    array_setting, ram_settings, 
    layer_name, layer_data, 
    directory, dim_vertical):

    base_trace_file = directory+"/"+str(dim_vertical)+"base_read.csv"
    bram_trace_file = directory+"/"+str(dim_vertical)+"bram_read.csv"
    uram_trace_file = directory+"/"+str(dim_vertical)+"uram_read.csv"

    base_cycles, util = base_trace.sram_traffic(
        dimension_rows = array_setting['ArrayHeight'][0],
        dimension_cols = dim_vertical,

        ifmap_h = layer_data['ifmap_h'],
        ifmap_w = layer_data['ifmap_w'],
        
        filt_h = layer_data['filt_h'],
        filt_w = layer_data['filt_w'],

        num_channels    = layer_data['num_channels'],
        strides     = layer_data['strides'],
        num_filt    = layer_data['num_filters'],

        ofmap_base  = array_setting['OfmapOffset'], 
        filt_base   = array_setting['FilterOffset'], 
        ifmap_base  = array_setting['IfmapOffset'],

        sram_read_trace_file = base_trace_file,
        sram_write_trace_file = "sram_write.csv",

        tqdm_enable=False)

    bram_trace.bram_trace_read(
        internal_ram_sz = ram_settings['BRAM']['InternalRamSize'] * array_setting['ArrayHeight'][0] * 4, #Byte
        word_sz_bytes = 1,

        ifmap_min_addr  = array_setting['IfmapOffset'], 
        ifmap_max_addr  = array_setting['FilterOffset'],
        filter_min_addr = array_setting['FilterOffset'], 
        filter_max_addr = array_setting['OfmapOffset'],

        ifmap_in_bw     = ram_settings['BRAM']['IfmapInBW'], 
        ifmap_out_bw    = ram_settings['BRAM']['IfmapOutBW'],
        filter_in_bw    = ram_settings['BRAM']['FilterInBW'],
        filter_out_bw   = ram_settings['BRAM']['FilterOutBW'],

        base_trace_file = base_trace_file,
        bram_trace_file = bram_trace_file)

    uram_trace.uram_trace_read(
        internal_ram_sz = ram_settings['URAM']['InternalRamSize'] * array_setting['ArrayHeight'][0],
        word_sz_bytes = 1,

        ifmap_min_addr  = array_setting['IfmapOffset'], 
        ifmap_max_addr  = array_setting['FilterOffset'],
        filter_min_addr = array_setting['FilterOffset'], 
        filter_max_addr = array_setting['OfmapOffset'],

        input_bw        = ram_settings['URAM']['IfmapInBW'],
        ifmap_out_bw    = ram_settings['URAM']['IfmapOutBW'], 
        filter_out_bw   = ram_settings['URAM']['FilterOutBW'],

        bram_trace_file = bram_trace_file,
        uram_trace_file = uram_trace_file)

    # units = " Bytes/cycle"
    # print("Average utilization : \t"  + str(util) + " %")
    # print("Cycles for compute  : \t"  + str(sram_cycles) + " cycles")
    # print("BRAM IFMAP Read BW : \t" + str(bram_ifmap_bw) + units)
    # print("URAM IFMAP Read BW : \t" + str(uram_ifmap_bw) + units)
    # print("Delta clock: " + str(delta_clk))

    delta_clk, bram_ifmap_bw, uram_ifmap_bw = analysis.gen_bw_number(bram_trace_file, uram_trace_file)

    return dim_vertical, util, base_cycles, delta_clk, bram_ifmap_bw, uram_ifmap_bw

def core(array_config_file, dnn_topology_file, rm_trace_enable):

    run_name, array_setting, ram_settings, topology = p_cfg.parse_config(array_config_file, dnn_topology_file)
    
    out =  "====================================================\n"
    out += "*************** SCALE SIM UltraScale ***************\n"
    out += "====================================================\n"
    out += "Architechture File : " + array_config_file + "\n"
    out += "DNN Topology File  : " + dnn_topology_file + "\n"
    out += "====================================================\n\n"
    print(out)

    for layer in topology.keys():

        directory = "/media/harrychan/Storage/ubuntuFile/URA_simulation/"+run_name+"/"+layer
        csv_output_file = "/media/harrychan/Storage/ubuntuFile/URA_simulation/"+run_name+"_"+layer+".csv"
        setup_log_file_storage(directory)
        results = {}

        print("Simulating", layer, "on", array_setting['ArrayWidth'])

        pool = Pool(processes = os.cpu_count() - 4)
        p_sim = partial(
            run_simulation, array_setting, ram_settings, 
            layer, topology[layer], directory)

        for pro in pool.imap_unordered(p_sim, array_setting['ArrayWidth']):
            results[pro[0]] = pro[1:]
            print("Done with size", pro[0])
            if rm_trace_enable:
                clean_trace_file_storage(directory+"/"+str(pro[0])+"base_read.csv")
                clean_trace_file_storage(directory+"/"+str(pro[0])+"bram_read.csv")
                clean_trace_file_storage(directory+"/"+str(pro[0])+"uram_read.csv")
        pool.close()
        pool.join()

        out = ""

        summary = []
        
        for size, result in results.items():
            out = "\n==== Report on size :" + str(size) + " ====\n"
            out += "Base Stat | Util:" + str(result[0]) + "%, Base Cycle: " + str(result[1]) + "\n"
            out += "Mem Stat  | Clk: " + str(result[2]) + ", BRAM BW: " + str(result[3]) + ", URAM BW: " + str(result[4]) + "\n"

            summary.append([size, result[0], result[1], result[2][0], result[4], result[2][1], result[3]])

            print(out)

        analysis.generate_csv(summary, csv_output_file)
    
    out =  "====================================================\n"
    out += "*************** Simulations END ***************\n"
    out += "====================================================\n"
    print(out)

if __name__ == "__main__":
    core("configs/UltraScale.cfg", "topologies/alexnet.csv")

    # python3 scale.py -arch_config=configs/UltraScale.cfg -network=topologies/conv_nets/Googlenet.csv -ultraScale=True
    # python3 scale.py -arch_config=configs/UltraScale.cfg -network=topologies/alexnet.csv -ultraScale=True
