import os.path
import math
from tqdm import tqdm

import conv_ultrascale_trace_gen as trace_utility

def bram_trace_read(
        internal_ram_sz = 18000, #Byte
        word_sz_bytes = 1,
        ifmap_min_addr=0, ifmap_max_addr=1000000,
        filter_min_addr=1000000, filter_max_addr=2000000,
        ifmap_in_bw = 4, ifmap_out_bw = 2, #Byte
        filter_in_bw = 4, filter_out_bw = 1, #Byte
        base_trace_file = "sram_read.csv",
        bram_trace_file = "ultra_bram_read.csv"
    ):

    ifmap_content = set()
    filter_content = set()

    filter_filled = False
    ifmap_filled = False
    filter_filling = False
    ifmap_filling = False
    cycle_accumulate = -1

    base_requests = open(base_trace_file, 'r')
    clear = open(bram_trace_file, 'w')
    clear.close

    #for entry in tqdm(base_requests):
    for entry in base_requests:
        clk, elems = trace_utility.turn_string_to_elements(entry)

        #fill filter
        if filter_min_addr <= elems[0] < filter_max_addr and not filter_filled:
            for e in range(0, len(elems)):
                filter_content.add(int(elems[e]))
            filter_filling = True
        else:
            filter_filled = True and filter_filling
        
        #fill ifmap
        if ifmap_min_addr <= elems[0] < ifmap_max_addr and not ifmap_filled:
            for e in range(0, len(elems)):
                ifmap_content.add(int(elems[e]))
            ifmap_filling = True
        else:
            ifmap_filled = True and filter_filled and ifmap_filling

        #print ifmap and filter
        if filter_filled and ifmap_filled:
            cycle_accumulate = trace_utility.gen_bram_trace(
                ifmap_addr = sorted(ifmap_content,reverse = True), 
                #filter_addr = filter_content,
                filter_addr = sorted(filter_content,reverse = True),
                ifmap_ram_size = internal_ram_sz,
                word_sz = 1,
                ifmap_in_bw = ifmap_in_bw, ifmap_out_bw = ifmap_out_bw,
                filter_in_bw = filter_in_bw, filter_out_bw = filter_out_bw,
                last_cycle = cycle_accumulate,
                output_file = bram_trace_file)
            filter_filled = False
            ifmap_filled = False
            filter_content.clear
            ifmap_content.clear
        
    if filter_filled and not ifmap_filled and len(ifmap_content) != 0:
        cycle_accumulate = trace_utility.gen_bram_trace(
            ifmap_addr = sorted(ifmap_content,reverse = True), 
            filter_addr = sorted(filter_content,reverse = True),
            ifmap_ram_size = internal_ram_sz,
            word_sz = 1,
            ifmap_in_bw = ifmap_in_bw, ifmap_out_bw = ifmap_out_bw,
            filter_in_bw = filter_in_bw, filter_out_bw = filter_out_bw,
            last_cycle = cycle_accumulate,
            output_file = bram_trace_file)
        filter_content.clear
        ifmap_content.clear

    base_requests.close()

# row based partition
if __name__ == "__main__":
    # bram_trace_read(
    #     internal_ram_sz = math.floor((18000 - 16)/8 * 3), #Byte
    #     word_sz_bytes = 1,
    #     ifmap_min_addr=0, ifmap_max_addr=10000000,
    #     filter_min_addr=10000000, filter_max_addr=20000000,
    #     ifmap_in_bw = 4, ifmap_out_bw = 2, #Byte
    #     filter_in_bw = 4, filter_out_bw = 1, #Byte
    #     base_trace_file = "test_in/googlenet_conv1_sram_read.csv",
    #     bram_trace_file = "test_out/ultra_bram_read_g1.csv")
    
    # bram_trace_read(
    #     internal_ram_sz = math.floor((18000 - 16)/8 * 3), #Byte
    #     word_sz_bytes = 1,
    #     ifmap_min_addr=0, ifmap_max_addr=10000000,
    #     filter_min_addr=10000000, filter_max_addr=20000000,
    #     ifmap_in_bw = 4, ifmap_out_bw = 2, #Byte
    #     filter_in_bw = 4, filter_out_bw = 1, #Byte
    #     base_trace_file = "test_in/simple_case_sram_read.csv",
    #     bram_trace_file = "test_out/ultra_bram_read_sc.csv")

    # bram_trace_read(
    #     internal_ram_sz = math.floor((18000 - 16)/8 * 3), #Byte
    #     word_sz_bytes = 1,
    #     ifmap_min_addr=0, ifmap_max_addr=10000000,
    #     filter_min_addr=10000000, filter_max_addr=20000000,
    #     ifmap_in_bw = 4, ifmap_out_bw = 2, #Byte
    #     filter_in_bw = 4, filter_out_bw = 1, #Byte
    #     base_trace_file = "test_in/simple_case_multiple_filters_sram_read.csv",
    #     bram_trace_file = "test_out/ultra_bram_read_scmf.csv")

    bram_trace_read(
        internal_ram_sz = math.floor((18000 - 16)/8 * 3), #Byte
        word_sz_bytes = 1,
        ifmap_min_addr=0, ifmap_max_addr=10000000,
        filter_min_addr=10000000, filter_max_addr=20000000,
        ifmap_in_bw = 4, ifmap_out_bw = 2, #Byte
        filter_in_bw = 4, filter_out_bw = 1, #Byte
        base_trace_file = "Googlenet_Conv1_sram_read.csv",
        bram_trace_file = "test_out/Google_Conv1_bram_read_g1.csv")