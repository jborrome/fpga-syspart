import os.path
import math

def prune(input_list):
    l = []

    for e in input_list:
        e = e.strip()
        if e != '' and e != ' ':
            l.append(e)

    return l

def turn_string_to_elements(inputString):
    elems = inputString.strip().split(',')
    elems = prune(elems)
    elems = [int(x) for x in elems]

    return elems[0], elems[1:]

def gen_empty_trace(start_cycle, end_cycle, output_file = "test.csv"):
    if start_cycle != end_cycle:
        out = open(output_file, 'a')
        trace = ""

        while start_cycle < end_cycle:
            trace += str(start_cycle) + ", " + "\n"
            start_cycle += 1

        out.write(trace)
        out.close()


def gen_filter_trace(
    filter_addr = set(),
    in_bw = 1, out_bw = 1,
    word_sz = 1,
    last_fill_cycle = -1,
    output_file = "test.csv"):

    out = open(output_file, 'a')
    trace = ""

    while len(filter_addr) > 0:
        trace += str(last_fill_cycle) + ", "
        for _ in range(in_bw):
            if len(filter_addr) > 0:
                p = filter_addr.pop()
                trace += str(p) + ", "
        trace += "\n"
        last_fill_cycle += 1
    out.write(trace)

    out.close()
    return last_fill_cycle

def gen_ifmap_trace(
    ifmap_addr = set(),
    ram_size = (18000 - 16)/8, 
    word_sz = 1,
    last_fill_cycle = -1,
    in_bw = 4, out_bw = 1,
    output_file = "test.csv"):

    pipeline_cycle = math.floor(in_bw/out_bw)
    out = open(output_file, 'a')

    trace = ""

    fisrt_cycles_needed = math.ceil((min((ram_size), len(ifmap_addr))) / (in_bw * word_sz))
    t_c = last_fill_cycle

    # print to fill up only first batch
    while t_c <= int(fisrt_cycles_needed + last_fill_cycle - 1):
        trace += str(t_c) + ", "
        for _ in range(in_bw):
            if len(ifmap_addr) > 0:
                p = ifmap_addr.pop()
                trace += str(p) + ", "
        trace += "\n"
        t_c += 1
    out.write(trace)

    trace = ""
    while len(ifmap_addr) > 0:
        for _ in range(pipeline_cycle - 1):
            trace += str(t_c) + ",\n"
            t_c += 1

        trace += str(t_c) + ", "
        for _ in range(in_bw):
            if len(ifmap_addr) > 0:
                p = ifmap_addr.pop()
                trace += str(p) + ", "
        trace += "\n"
        t_c += 1

        if len(trace) > 2000:
            out.write(trace)
            trace = ""
    out.write(trace)

    out.close()
    return t_c

def gen_uram_trace(
    ifmap_addr = set(), filter_addr = list(),
    ifmap_ram_size = (18000 - 16) *3 /8, word_sz = 1,
    input_bw = 8,
    ifmap_out_bw = 4, filter_out_bw = 4,
    last_cycle = -1,
    patch_first_drain_clk = 0,
    first_ever_fetch = True,
    output_file = "test.csv"):

    filter_cycles = math.ceil(len(filter_addr) / (input_bw * word_sz))
    ifmap_cycles = math.ceil((min((ifmap_ram_size), len(ifmap_addr))) / (input_bw * word_sz))

    if first_ever_fetch:
        last_cycle  = last_cycle - filter_cycles - ifmap_cycles
    # else:
    #     fill_needed_cycle = patch_first_drain_clk - filter_cycles - ifmap_cycles
    #     fill_starting_cycle = max(fill_needed_cycle, last_cycle)
    #     gen_empty_trace(last_cycle, fill_starting_cycle, output_file)

    last_cycle = gen_filter_trace(    
        filter_addr = filter_addr,
        in_bw = input_bw, out_bw = filter_out_bw,
        word_sz = word_sz,
        last_fill_cycle = last_cycle,
        output_file = output_file)
    
    return gen_ifmap_trace(
        ifmap_addr = ifmap_addr,
        ram_size = ifmap_ram_size, 
        word_sz = word_sz,
        last_fill_cycle = last_cycle,
        in_bw = input_bw, out_bw = ifmap_out_bw,
        output_file = output_file)
    
def gen_bram_trace(
    ifmap_addr = set(), filter_addr = set(),
    ifmap_ram_size = (18000 - 16)/8,
    word_sz = 1,
    ifmap_in_bw = 4, ifmap_out_bw = 2,
    filter_in_bw = 1, filter_out_bw = 1,
    last_cycle = -1,
    output_file = "test.csv"
    ):

    if last_cycle == -1:
        filter_cycles = math.ceil(len(filter_addr) / (filter_in_bw * word_sz))
        ifmap_cycles = math.ceil((min((ifmap_ram_size), len(ifmap_addr))) / (ifmap_in_bw * word_sz))
        last_cycle  = last_cycle - filter_cycles - ifmap_cycles

    last_cycle = gen_filter_trace(    
        filter_addr = filter_addr,
        in_bw = filter_in_bw, out_bw = filter_out_bw,
        word_sz = word_sz,
        last_fill_cycle = last_cycle,
        output_file = output_file)

    return gen_ifmap_trace(
        ifmap_addr = ifmap_addr,
        ram_size = ifmap_ram_size, 
        word_sz = word_sz,
        last_fill_cycle = last_cycle,
        in_bw = ifmap_in_bw, out_bw = ifmap_out_bw,
        output_file = output_file)