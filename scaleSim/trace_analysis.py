def gen_bw_number(  bram_trace_file, 
                    uram_trace_file):
    
    detailed_log = ""
    delta_clk = []

    # bramtrace
    min_clk = 100000
    max_clk = -1
    num_bram_bytes = 0
    f = open(bram_trace_file, 'r')
    start_clk = 0
    first = True

    for row in f:
        num_bram_bytes += len(row.split(',')) - 2
        
        elems = row.strip().split(',')
        clk = float(elems[0])

        if first:
            first = False
            start_clk = clk

        if clk < min_clk:
            min_clk = clk

        if max_clk < clk:
            max_clk = clk

    stop_clk = clk
    delta_clk.append(max_clk - min_clk)
    detailed_log += str(start_clk) + ",\t" + str(stop_clk) + ",\t" + str(num_bram_bytes) + ",\t"
    f.close()

    # uramtrace
    min_clk = 100000
    max_clk = -1
    num_uram_bytes = 0
    f = open(uram_trace_file, 'r')
    start_clk = 0
    first = True

    for row in f:
        num_uram_bytes += len(row.split(',')) - 2
        
        elems = row.strip().split(',')
        clk = float(elems[0])

        if first:
            first = False
            start_clk = clk

        if clk < min_clk:
            min_clk = clk

        if max_clk < clk:
            max_clk = clk

    stop_clk = clk
    delta_clk.append(max_clk - min_clk)
    detailed_log += str(start_clk) + ",\t" + str(stop_clk) + ",\t" + str(num_uram_bytes) + ",\t"
    f.close()

    #output trace analysis
    # num_dram_ofmap_bytes = 0
    # f = open(dram_ofmap_trace_file, 'r')
    # first = True

    # for row in f:
    #     num_dram_ofmap_bytes += len(row.split(',')) - 2

    #     elems = row.strip().split(',')
    #     clk = float(elems[0])

    #     if first:
    #         first = False
    #         start_clk = clk

    # stop_clk = clk
    # detailed_log += str(start_clk) + ",\t" + str(stop_clk) + ",\t" + str(num_dram_ofmap_bytes) + ",\t"
    # f.close()
    # if clk > max_clk:
    #     max_clk = clk

    # delta_clk = max_clk - min_clk

    bram_ifmap_bw  = num_bram_bytes / int(delta_clk[0])
    uram_ifmap_bw  = num_uram_bytes / int(delta_clk[1])

    return delta_clk, bram_ifmap_bw, uram_ifmap_bw

def generate_csv(results, csv_path):
    import numpy as np
    import pandas as pd
    from os import path
    from os import system
        
    df = pd.DataFrame(
        results, 
        columns = [ 'size',
                    'base util',
                    'base cycle',
                    'URAM cycle',
                    'URAM BW',
                    'BRAM cycle',
                    'BRAM BW'])

    if path.exists(csv_path):
        old_df = pd.read_csv(csv_path)
        system('rm -rf ' + csv_path)
        df = [old_df, df]
        df = pd.concat(df)

    df = df.sort_values(by=['size'], ascending=True)
    df.to_csv(csv_path, index = False)

if __name__ == "__main__":
    results = [
        [134, 12, 12, 12, 12, 12, 12],
        [123, 12, 12, 12, 12, 12, 12],
        [1223, 12, 12, 12, 12, 12, 12],
        [34, 12, 12, 12, 12, 12, 12],
        [85, 12, 12, 12, 12, 12, 12],
    ]
    generate_csv(results, "test_out/test.csv")