#!/bin/bash
# ./generate_data_set.sh configs/US_sim.cfg topologies/960_DNN/Alexnet.csv 

if [ "$1" != "" ]; then
    python3 scale.py -arch_config=$1 -network=$2 -ultraScale=False -enable_mp=True
else
    echo "Positional parameter 1 is empty"
fi