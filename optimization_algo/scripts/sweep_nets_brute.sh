#! /bin/bash

declare -a nets=(FasterRCNN mobilenet yolo_tiny googlenet alexnet AlphaGoZero ncf_rec resnet_50_v1)
declare -a layers=(46 27 10 58 8 8 8 53)
declare -a max_partitions=(7 7 7 7 7 7 7 7)

for i in 0 1 2 3 4 5 6 7;
do
    net=${nets[$i]}
    for partitions in `seq 3 ${max_partitions[$i]}`;
    do
        for res_unit in 960;
        do
            for target in DRAM_cycle;
	        do
                echo $net $partitions $target
                python3 ../approaches/brute_force_approach.py \
                    ${net} \
                    ${partitions} \
                    ${res_unit} \
		            ${target}
	        done
        done
    done
done
