#! /bin/bash

declare -a nets=(FasterRCNN mobilenet yolo_tiny googlenet alexnet AlphaGoZero ncf_rec resnet_50_v1)
declare -a layers=(46 27 10 58 8 8 8 53)
declare -a max_partitions=(23 14 9 29 8 8 8 27)

# echo "target, evo_counter,partitions,topology, feasable, \
# tp_partition, latency_partition, \
# tp_fullmap, latency_fm, \
# tp_gain, latency_penalty, \
# best_layer_partition, best_resource_partition, \
# time_taken, sigma, seed, valid_sampling_percentage, \
# trial, popsize, res_unit, seeding_type" > ../resulting_csv/hyperopt.csv

for i in 0 1 2 3 4 5 6 7;
do
    net=${nets[$i]}
    for partitions in `seq 3 ${max_partitions[$i]}`;
    do
        for res_unit in 960;
        do
            for target in DRAM_cycle Cycles;
	        do
                echo $net $partitions $target
                python3 ../approaches/hyper_parameter_ga.py \
                    ${net} \
                    ${partitions} \
                    ${res_unit} \
		            ${target} \
                    2500
	        done
        done
    done
done
