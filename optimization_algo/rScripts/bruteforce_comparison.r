library(ggplot2)
library(scales)
library(dplyr)
require(data.table)
library(xtable)
#setwd("~/Project/watcag/FPGA-Systolic-Modeling-Tool/partitioning_problem")

cma_data <- read.csv("cma.csv", header=TRUE, sep=",")
bf_data <- read.csv("bruteforce.csv", header=TRUE, sep=",")

# 1. best result of each mlperf benchmarks using cma with each partition size
cma_filtered_data <- subset(cma_data, feasable == 1 & target=="DRAM_cycle" & seeding_type == "optimised" & res_unit=="960")
topo_unique <- unique(cma_filtered_data$topology)
i <- 1
temp_cma <- list()

for (topo in topo_unique) {
  d_cma <- subset(cma_filtered_data, topology == topo) %>% group_by(partitions) %>% top_n(1, evo_counter) %>% select(topology, partitions, tp_partition, tp_gain, latency_partition, latency_penalty, time_taken)
  temp_cma[[i]] <- d_cma[order(d_cma$partitions),]
  i <- i+1
}

cma_final_data <- rbindlist(temp_cma) %>% distinct(topology, partitions, .keep_all= TRUE)
cma_table <- cma_final_data %>% select(topology, partitions, tp_partition, tp_gain, latency_partition, latency_penalty)
xtable(cma_table, display=c("d","s","d","g","g","g","g"), math.style.exponents = TRUE)

# 2. best result of each mlperf benchmarks using bruteforce with each partition size
bf_filtered_data <- subset(bf_data, feasable == 1 & target=="DRAM_cycle" & partitions > 2)
topo_unique <- unique(bf_filtered_data$topology)
i <- 1
temp_bf <- list()

for (topo in topo_unique) {
  d_bf <- subset(bf_filtered_data, topology == topo) %>% select(topology, partitions, tp_partition, tp_gain, latency_partition, latency_penalty, time_taken)
  temp_bf[[i]] <- d_bf[order(d_bf$partitions),]
  i <- i+1
}

bf_final_data <- rbindlist(temp_bf) %>% distinct(topology, partitions, .keep_all= TRUE)
bf_table <- bf_final_data %>% select(topology, partitions, tp_partition, tp_gain, latency_partition, latency_penalty)
xtable(bf_table, display=c("d","s","d","g","g","g","g"), math.style.exponents = TRUE)

# 3. best result for CMA and BF across all partition sizes <7
cma_compare <- subset(cma_final_data, partitions <= 7) %>% distinct(topology, partitions, .keep_all= TRUE) %>% select(topology, partitions, latency_partition, time_taken)
bf_compare <- bf_filtered_data %>% select(topology, partitions, latency_partition, time_taken) %>% distinct(topology, partitions, .keep_all= TRUE)

cma_v_bf <- merge(cma_compare, bf_compare, by=c("topology", "partitions"))
#names(cma_v_bf)[names(cma_v_bf) == 'tp_partition.x'] <- 'cma_tp'
#names(cma_v_bf)[names(cma_v_bf) == 'tp_partition.y'] <- 'bf_tp'

names(cma_v_bf)[names(cma_v_bf) == 'latency_partition.x'] <- 'cma_latency'
names(cma_v_bf)[names(cma_v_bf) == 'latency_partition.y'] <- 'bf_latency'

names(cma_v_bf)[names(cma_v_bf) == 'time_taken.x'] <- 'cma_time'
names(cma_v_bf)[names(cma_v_bf) == 'time_taken.y'] <- 'bf_time'

cma_v_bf = mutate(cma_v_bf, 
                  latency_diff = (cma_latency - bf_latency) *100 / bf_latency,
                  time_diff = (cma_time - bf_time) *100 / bf_time)

names(cma_v_bf)[names(cma_v_bf) == 'latency_diff'] <- 'latency_diff(%)'
names(cma_v_bf)[names(cma_v_bf) == 'time_diff'] <- 'time_diff(%)'

xtable(cma_v_bf, display=c("d","s","d","g","g","g","g","g","g"), math.style.exponents = TRUE)



