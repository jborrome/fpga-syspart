library(ggplot2)
library(scales)

ps=3
ats=25
ts=25

partition=8
data <- read.csv("cma.csv", header=TRUE, sep=",")

pdf(file="serial-vs-parallel.pdf", height=4, width=6)
f_data <- subset(data, feasable == 1 & target=="DRAM_cycle" & seeding_type=="optimised" & partitions==partition & topology=="googlenet.csv")

filtered_data <- data.frame(matrix(ncol=7,nrow=0))
col_headings <- c("type", "topology", "partition", "tp_gain", "evo_counter","latency_penalty", "time")
names(filtered_data) <- col_headings

topo_unique<-unique(f_data$topology)
partition_unique<-unique(f_data$partitions)
print(topo_unique)

for (topo in topo_unique)
{
    for (part in partition_unique)
    {
        mydata = subset(f_data, topology==topo & partitions==part)
        if (dim(mydata)[1]!=0)
        {
            max_evo = max(mydata$evo_counter)
            for (row in 1:nrow(mydata))
            {
              filtered_data <- rbind(filtered_data, data.frame("type"="parallel","topology"=topo,"partitions"=part, "tp_gain"=mydata$tp_gain[row],"evo_counter"=mydata$evo_counter[row], "latency_penalty"=mydata$latency_penalty[row], "time"=mydata$time_taken[row]))
            }
        }
    }
}

data <- read.csv("cma_serial.csv", header=TRUE, sep=",")
f_data <- subset(data, feasable == 1 & target=="DRAM_cycle" & partitions==partition & topology=="googlenet.csv")

topo_unique<-unique(f_data$topology)
partition_unique<-unique(f_data$partitions)
print(topo_unique)

for (topo in topo_unique)
{
    for (part in partition_unique)
    {
        mydata = subset(f_data, topology==topo & partitions==part)
        if (dim(mydata)[1]!=0)
        {
            max_evo = max(mydata$evo_counter)
            for (row in 1:nrow(mydata))
            {
              filtered_data <- rbind(filtered_data, data.frame("type"="serial","topology"=topo,"partitions"=part, "tp_gain"=mydata$tp_gain[row],"evo_counter"=mydata$evo_counter[row], "latency_penalty"=mydata$latency_penalty[row], "time"=mydata$time_taken[row]))
            }
        }
    }
}

print(filtered_data)

p <- ggplot(filtered_data,aes(y=as.numeric(tp_gain), x=as.numeric(time), group=type))
p <- p + geom_point(size=ps, aes(color=type, shape=type))
p <- p + geom_line(aes(color=type))
p<-p+labs(x="Time Spent (s)", y="Throughput Gain")
p<-p+theme_bw()
p<-p+facet_grid(~topology)
p<-p+theme(axis.text=element_text(size=ats))
p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="bottom")
p
