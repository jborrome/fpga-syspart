data <- read.csv("googlenet_data.csv", header=TRUE, sep=",")

p <- ggplot(data,aes(y=as.numeric(tp_gain), x=as.numeric(partitions), group=topology))
p <- p + geom_point(size=ps, aes(color=topology, shape=topology))
p <- p + geom_line(aes(color=topology))
p<-p+labs(x="No of Partitions", y="Throughput Gain")
p<-p+theme_bw()
p<-p+facet_grid(~topology)
p<-p+theme(axis.text=element_text(size=ats))
p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="bottom")
p