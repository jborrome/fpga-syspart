library(ggplot2)
library(scales)

ps=3
ats=25
ts=25

partition=5
data <- read.csv("cma.csv", header=TRUE, sep=",")

pdf(file="tp-iter", height=4, width=8)
f_data <- subset(data, feasable == 1 & target=="DRAM_cycle" & seeding_type=="optimised" & partitions==partition)

filtered_data <- data.frame(matrix(ncol=6,nrow=0))
col_headings <- c("topology", "partition", "tp_gain", "evo_counter","latency_penalty", "time")
names(filtered_data) <- col_headings

topo_unique<-unique(f_data$topology)
partition_unique<-unique(f_data$partitions)
print(topo_unique)

for (topo in topo_unique)
{
    for (part in partition_unique)
    {
        mydata = subset(f_data, topology==topo & partitions==part)
        if (dim(mydata)[1]!=0)
        {
            max_evo = max(mydata$evo_counter)
            for (row in 1:nrow(mydata))
            {
              filtered_data <- rbind(filtered_data, data.frame("topology"=topo,"partitions"=part, "tp_gain"=mydata$tp_gain[row],"evo_counter"=mydata$evo_counter[row], "latency_penalty"=mydata$latency_penalty[row], "time"=mydata$time_taken[row]))
            }
        }
    }
}


print(filtered_data)
filtered_data<-filtered_data[filtered_data$topology=="googlenet.csv",]
write.table(filtered_data,row.names=F,quote=F,"googlenet_gain_fortikz.csv")
quit()

p <- ggplot(filtered_data,aes(y=as.numeric(tp_gain), x=as.numeric(evo_counter), group=topology))
p <- p + geom_point(size=ps, aes(color=topology, shape=topology))
p <- p + geom_line(aes(color=topology))
p<-p+labs(x="Evolution Iteration", y="Throughput Gain")
p<-p+theme_bw()
p<-p+facet_grid(~topology)
p<-p+theme(axis.text=element_text(size=ats))
p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="bottom")
p
