library(ggplot2)
library(scales)

ps=3
ats=25
ts=25

partition=10
topo="googlenet.csv"
data <- read.csv("cma.csv", header=TRUE, sep=",")

pdf(file="tp-latency_evolution.pdf", height=4, width=6)
f_data <- subset(data, feasable == 1 & target=="DRAM_cycle" & seeding_type=="optimised" & partitions==partition & topology==topo)


f_data$latency_penalty <- factor(f_data$latency_penalty, levels=f_data$latency_penalty[order(f_data$evo_counter)])

p <- ggplot(f_data,aes(y=as.numeric(tp_gain), x=as.numeric(latency_penalty), group=topology))
p <- p + geom_point(size=ps, aes(color=as.factor(evo_counter), shape=topology))
p <- p + geom_line(aes(color=as.factor(evo_counter)))
p<-p+labs(x="Latency Penalty", y="Throughput Gain")
p<-p+theme_bw()
p<-p+theme(axis.text=element_text(size=ats))
p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="None")
p
