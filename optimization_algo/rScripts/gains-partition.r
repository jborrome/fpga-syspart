library(ggplot2)
library(scales)

ps=3
ats=25
ts=25

data <- read.csv("cma.csv", header=TRUE, sep=",")

pdf(file="gains-partition.pdf", height=4, width=48)
f_data <- subset(data, feasable == 1 & target=="DRAM_cycle" & seeding_type=="optimised")

filtered_data <- data.frame(matrix(ncol=4,nrow=0))
col_headings <- c("topology", "partitions", "tp_gain", "latency_penalty")
names(filtered_data) <- col_headings

topo_unique<-unique(f_data$topology)
partition_unique<-unique(f_data$partitions)
print(topo_unique)

for (topo in topo_unique)
{
    for (part in partition_unique)
    {
        mydata = subset(f_data, topology==topo & partitions==part)
        if (dim(mydata)[1]!=0)
        {
            max_evo = max(mydata$evo_counter)
            row = which(mydata$evo_counter==max_evo)
            print(max_evo)
            print(mydata$tp_gain[row])
            filtered_data <- rbind(filtered_data, data.frame("topology"=topo,"partitions"=part, "tp_gain"=mydata$tp_gain[row], "latency_penalty"=mydata$latency_penalty[row]))
        }
    }
}


print(filtered_data)
write.table(filtered_data,row.names=F,quote=F,"tput_latency_vs_partition.csv")

p <-    ggplot(filtered_data,aes(y=as.numeric(tp_gain), x=as.numeric(partitions), group=topology))
p <- p + scale_shape_manual(values=1:nlevels(filtered_data$topology))
p <- p + geom_point(size=ps, aes(color=topology))
p <- p + geom_line(aes(color=topology))
p<-p+labs(x="No of Partitions", y="Throughput Gain")
p<-p+theme_bw()
p<-p+facet_grid(~topology)
p<-p+theme(axis.text=element_text(size=ats))
p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="bottom")
p

p <- ggplot(filtered_data,aes(y=as.numeric(latency_penalty), x=as.numeric(partitions), group=topology))
p <- p + scale_shape_manual(values=1:nlevels(filtered_data$topology))
p <- p + geom_point(size=ps, aes(color=topology))
p <- p + geom_line(aes(color=topology))
p<-p+labs(x="No of Partitions", y="Latency Penalty")
p<-p+theme_bw()
p<-p+facet_grid(~topology)
p<-p+theme(axis.text=element_text(size=ats))
p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="bottom")
p

p <- ggplot(filtered_data,aes(y=as.numeric(tp_gain), x=as.numeric(latency_penalty), group=topology))
p <- p + scale_shape_manual(values=1:nlevels(filtered_data$topology))
p <- p + geom_point(size=ps, aes(color=topology))
p<-p+labs(x="Latency Penalty", y="Throughput Gain")
p<-p+theme_bw()
p<-p+facet_grid(~topology)
p<-p+theme(axis.text=element_text(size=ats))
p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="bottom")
p

