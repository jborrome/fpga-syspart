library(ggplot2)
library(scales)
library(stringr)

ps=3
text_size=18
axis_text_size=25

partition = 5

data <- read.csv("./cma.csv", header=TRUE, sep=",")

pdf(file="resource-evolution_no.pdf", height=4, width=6)

data <- subset(data, topology=="googlenet.csv" & feasable==1 & seeding_type=="optimised" & target=="DRAM_cycle")

data_f <- subset(data, partitions==partition)
data_f <- subset(data_f, trial == max(data_f$trial))

filtered_data <- data.frame(matrix(ncol=3,nrow=0))
col_headings <- c("evo_counter", "layer", "resource")
names(filtered_data) <- col_headings

for (row in 1:nrow(data_f))
{
  evolution_counter <- data_f$evo_counter[row]
  resource_partition <- data_f$best_resource_partition[row]
  #print(resource_partition)
  resource_partition <- gsub("\\[|\\]", "", resource_partition)
  resource_partition <- str_split_fixed(resource_partition,", ", partition)
  for (i in 1:partition)
  {
    filtered_data <- rbind(filtered_data, data.frame("evo_counter"=evolution_counter,"layer"=i,"resource"=as.numeric(resource_partition[i])))
  }
}

print(filtered_data)
write.table(filtered_data,row.names=F,quote=F,"googlenet_resource_fortikz.csv")

p <- ggplot()
p <- p + geom_col(data=filtered_data, aes(x=evo_counter, y=resource, fill=as.factor(layer)))
p <- p + labs(x="No of Evolutions", y="Resources")
p <- p + theme_bw()
p <- p + theme(axis.text=element_text(size=axis_text_size))
p <- p + scale_y_continuous(limits=c(0,970), breaks=c(120,480,960))
p <- p + theme(text=element_text(size=text_size))
p <- p + theme(legend.position="None")
p

partition=5

data_f <- subset(data, partitions==partition)
data_f <- subset(data_f, trial == max(data_f$trial))

filtered_data <- data.frame(matrix(ncol=3,nrow=0))
col_headings <- c("evo_counter", "layer", "resource")
names(filtered_data) <- col_headings

for (row in 1:nrow(data_f))
{
  evolution_counter <- data_f$evo_counter[row]
  resource_partition <- data_f$best_resource_partition[row]
  #print(resource_partition)
  resource_partition <- gsub("\\[|\\]", "", resource_partition)
  resource_partition <- str_split_fixed(resource_partition,", ", partition)
  for (i in 1:partition)
  {
    filtered_data <- rbind(filtered_data, data.frame("evo_counter"=evolution_counter,"layer"=i,"resource"=as.numeric(resource_partition[i])))
  }
}

print(filtered_data)

p <- ggplot()
p <- p + geom_col(data=filtered_data, aes(x=evo_counter, y=resource, fill=as.factor(layer)))
p <- p + labs(x="No of Evolutions", y="Resources")
p <- p + theme_bw()
p <- p + theme(axis.text=element_text(size=axis_text_size))
p <- p + scale_y_continuous(limits=c(0,970), breaks=c(120,480,960))
p <- p + theme(text=element_text(size=text_size))
p <- p + theme(legend.position="None")
p

partition=10

data_f <- subset(data, partitions==partition)
data_f <- subset(data_f, trial == max(data_f$trial))

filtered_data <- data.frame(matrix(ncol=3,nrow=0))
col_headings <- c("evo_counter", "layer", "resource")
names(filtered_data) <- col_headings

for (row in 1:nrow(data_f))
{
  evolution_counter <- data_f$evo_counter[row]
  resource_partition <- data_f$best_resource_partition[row]
  #print(resource_partition)
  resource_partition <- gsub("\\[|\\]", "", resource_partition)
  resource_partition <- str_split_fixed(resource_partition,", ", partition)
  for (i in 1:partition)
  {
    filtered_data <- rbind(filtered_data, data.frame("evo_counter"=evolution_counter,"layer"=i,"resource"=as.numeric(resource_partition[i])))
  }
}

print(filtered_data)

p <- ggplot()
p <- p + geom_col(data=filtered_data, aes(x=evo_counter, y=resource, fill=as.factor(layer)))
p <- p + labs(x="No of Evolutions", y="Resources")
p <- p + theme_bw()
p <- p + theme(axis.text=element_text(size=axis_text_size))
p <- p + scale_y_continuous(limits=c(0,970), breaks=c(120,480,960))
p <- p + theme(text=element_text(size=text_size))
p <- p + theme(legend.position="None")
p
