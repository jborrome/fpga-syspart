library(ggplot2)
library(scales)

ps=3
text_size=18
axis_text_size=25

partition = 5
target_col = "DRAM_cycle"

data <- read.csv("./cma.csv", header=TRUE, sep=",")

pdf(file="valid_sol-evolution_no.pdf", height=4, width=18)

topo_unique <- unique(data$topology)


mydata<-data[FALSE,]
for (topo in topo_unique)
{
    data_f <- subset(data, partitions == partition & topology == topo & target == target_col)
    if (dim(data_f)[1]!=0)
    {
        mydata <- rbind(mydata, subset(data_f,trial == max(data_f$trial) ))
    }
}

mydata2<-data.frame(topology=mydata$topology,evo_counter=mydata$evo_counter,valid_sampling_percentage=mydata$valid_sampling_percentage,seeding_type=mydata$seeding_type)
write.table(mydata2,row.names=F,quote=F,"valid_sols.csv")

p <- ggplot(mydata, aes(x=evo_counter, y=valid_sampling_percentage))
p <- p + geom_point(aes(shape=seeding_type, fill=seeding_type,
         color=seeding_type), size=ps)
p <- p + geom_line(aes(color=seeding_type))
p <- p + labs(x="No of Evolutions", y="% Valid Solutions")
p <- p + theme_bw()
p <- p + facet_grid(~topology)
p <- p + theme(axis.text=element_text(size=axis_text_size))
p <- p + scale_y_continuous(limits=c(0,102), breaks=c(0,25,50,75,100))
p <- p + theme(text=element_text(size=text_size))
p <- p + theme(legend.position="bottom")
p
