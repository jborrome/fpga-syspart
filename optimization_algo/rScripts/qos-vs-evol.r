library(ggplot2)
library(scales)

ps=3
ats=25
ts=25

partition=10
data1 <- read.csv("cma.csv", header=TRUE, sep=",")
data1$mltype<-"cma"
data2 <- read.csv("ga.csv", header=TRUE, sep=",")
data2$mltype<-"ga"
data2$seeding_type<-"optimised"

data<-rbind(data1,data2)
#data<-data2

pdf(file="qos-vs-evol.pdf", height=4, width=16)

f_data <- subset(data, feasable == 1 & target=="DRAM_cycle" & seeding_type=="optimised" & partitions==partition)

filtered_data <- data.frame(matrix(ncol=7,nrow=0))
col_headings <- c("topology", "partition", "tp_gain", "evo_counter","latency_penalty", "time","mltype")
names(filtered_data) <- col_headings

topo_unique<-unique(f_data$topology)
partition_unique<-unique(f_data$partitions)
print(topo_unique)

for (topo in topo_unique)
{
  for (part in partition_unique)
  {
    for (ml in c("cma","ga"))
    {
      mydata = subset(f_data, topology==topo & partitions==part & mltype==ml)
      if (dim(mydata)[1]!=0)
      {
        max_evo = max(mydata$evo_counter)
        for (row in 1:nrow(mydata))
        {
          filtered_data <- rbind(filtered_data, data.frame("topology"=topo,"partitions"=part, "mltype"=ml, "tp_gain"=mydata$tp_gain[row],"evo_counter"=mydata$evo_counter[row], "latency_penalty"=mydata$latency_penalty[row], "time"=mydata$time_taken[row]))
        }
      }
    }
  }
}


print(filtered_data)
write.table(filtered_data,row.names=F,quote=F,"qos-vs-evol.csv")

p <- ggplot(filtered_data,aes(y=as.numeric(tp_gain), x=as.numeric(time), group=mltype))
p <- p + geom_point(size=ps, aes(color=mltype, shape=mltype))
p <- p + geom_line(aes(color=mltype))
p<-p+labs(x="Evolution Iteration", y="Throughput Gain")
p<-p+theme_bw()
p<-p+facet_grid(~topology)
p<-p+theme(axis.text=element_text(size=ats))
p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="bottom")
p
