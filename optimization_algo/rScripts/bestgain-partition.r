library(ggplot2)
library(scales)

ps=7
ats=25
ts=25

data <- read.csv("cma.csv", header=TRUE, sep=",")

pdf(file="bestgain-partition.pdf", height=4, width=6)
f_data <- subset(data, feasable == 1 & target=="DRAM_cycle" & seeding_type=="optimised")

column_data <- data.frame(matrix(ncol=5,nrow=0))
col_headings <- c("topology", "partitions", "tp_gain", "latency_penalty", "ratio")
names(column_data) <- col_headings

topo_unique<-unique(f_data$topology)
partition_unique<-unique(f_data$partitions)

for (topo in topo_unique)
{
    for (part in partition_unique)
    {
        mydata = subset(f_data, topology==topo & partitions==part)
        if (dim(mydata)[1]!=0)
        {
            max_evo = max(mydata$evo_counter)
            row = which(mydata$evo_counter==max_evo)
            column_data <- rbind(column_data, data.frame("topology"=topo,"partitions"=part, "tp_gain"=mydata$tp_gain[row], "latency_penalty"=mydata$latency_penalty[row], "ratio"=mydata$tp_gain[row]/mydata$latency_penalty[row]))
        }
    }
}

filtered_data <- column_data[FALSE,]
topo_unique <- unique(column_data$topology)

for (topo in topo_unique)
{
    mydata = subset(column_data, topology == topo)
    row = which(mydata$ratio==max(mydata$ratio))
    filtered_data <- rbind(filtered_data, mydata[row,])
}

write.table(filtered_data,row.names=F,quote=F,"bestgain.csv")


p <- ggplot(filtered_data,aes(y=as.numeric(ratio), x=as.numeric(partitions), group=topology))
p <- p + geom_point(size=ps, aes(color=topology, shape=topology))
p<-p+labs(x="Number of Partitions", y="Best Overall Gain")
p<-p+theme_bw()
p<-p+theme(axis.text=element_text(size=ats))
p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="bottom")
p
